#!/usr/bin/env python
# Métodos comunes comunes para los scripts de Archivo de la Palabra
# (c) 2023 perro tuerto <hi@perrotuerto.blog>
# Financiado por la Academia Mexicana de la Lengua <https://academia.org.mx>
# Este código es software libre con licencia GPLv3

import os
from typing import Any
from pathlib import Path
from datetime import datetime


class Log:
    """Imprime o guarda los registros de las actividades realizadas

    :param quiet: Indica si imprime el registro, False por defecto
    :type quiet: bool, opcional
    :param write: Indica si guarda el registro en un archivo, False por defecto
    :type write: bool, opcional
    """

    def __init__(self, quiet: bool = False, write: bool = False) -> None:
        self.quiet, self.write = quiet, write
        self.timestamp = "[%s]" % datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
        if write:
            self.logfile = Path.cwd() / "log"

    def puts(
        self, *chunks: Any, level: str = "info", clean: bool = False
    ) -> str:
        """Imprime el registro

        :param *chunks: Fragmentos para el cuerpo del mensaje
        :type *chunks: Any
        :param level: Nivel de depuración entre "info", "warn" o "error",
            "info" por defecto
        :type level: str, opcional
        :param clean: Indica si la impresión ha de limpiarse cuando se imprima
            otra línea, False por defecto
        :type clean: bool, opcional
        :return: Mensaje del registro
        :rtype: str
        """
        body = " ".join(list(map(lambda x: str(x), [*chunks])))
        msg = "[%s] %s" % (level.upper(), body)
        if not self.quiet or level == "error":
            width = os.get_terminal_size()[0]
            blank = width * " "
            end = "\r" if clean else "\n"
            print(blank, end="\r")
            msg = msg[:width] if clean and len(msg) > width else msg
            print(msg, end=end)
        if self.write:
            self.__write(msg)
        if level == "error":
            exit(1)
        return msg

    def __write(self, *chunks: Any) -> None:
        """Guarda el registro. La ubicación del archivo es la misma a la
        ubicación actual en la terminal

        :param *chunks: Fragmentos para el cuerpo del mensaje
        :type *chunks: Any
        """
        mode = "a" if self.logfile.exists() else "w"
        file = open(self.logfile, mode)
        msg = " ".join([self.timestamp, *chunks])
        print(msg, file=file)
        file.close()


class Event:
    """Crea un nuevo evento cuando se aplana un XML para obtener una estructura
    basada en eventos (similar a XML SAX, pero más simplificada)

    :param type: Tipo de evento entre INITAG', 'ENDTAG', 'STRING', 'TOKEN' o
        'NONE'
    :type type: str
    :param value: Valor del evento
    :type value: str
    :param attrs: Diccionario de atributos, {} por defecto
    :type attrs: dict
    """

    def __init__(
        self, level: int, type: str, value: str, attrs: dict = {}
    ) -> None:
        self.level = level
        self.type = type
        self.value = value
        self.attrs = attrs

    def __str__(self) -> str:
        """Imprime Evento como tupla"""
        return str((self.level, self.type, self.value, self.attrs))
