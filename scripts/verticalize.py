#!/usr/bin/env python
# Conversión de XML a VRT de Archivo de la Palabra
# (c) 2023 perro tuerto <hi@perrotuerto.blog>
# Financiado por la Academia Mexicana de la Lengua <https://academia.org.mx>
# Este código es software libre con licencia GPLv3
# python scripts/verticalize.py

import csv
import yaml
import spacy
import bleach
import xml.etree.ElementTree as ET
from pathlib import Path
from common import Log, Event
from bs4 import BeautifulSoup, Tag, NavigableString


class Verticalize:
    """Convierte y escribe archivos XML a VRT, el formato de CPQweb

    :param in_dir: Directorio con los XML validados, permite subdirectorios,
        Path vacío por defecto
    :type in_dir: Path
    :param out_dir: Directorio para los VRT, Path vacío por defecto
    :type out_dir: Path
    :param metadata: Archivo para los metadatos, Path vacío por defecto
    :type metadata: Path
    :param model: Modelo que usará spaCY; "es_core_news_lg" por defecto
    :type model: str
    :param quiet: Indica si imprime el registro, False por defecto
    :type quiet: bool, opcional
    :param write: Indica si guarda el registro en un archivo, False por defecto
    :type write: bool, opcional
    """

    def __init__(
        self,
        in_dir: Path = "",
        out_dir: Path = "",
        metadata: Path = "",
        model: str = "es_core_news_lg",
        quiet: bool = False,
        write: bool = False,
    ) -> None:
        self.log = Log(quiet=quiet, write=write)
        self.in_dir = self.__inspect(in_dir, "--in-dir")
        self.out_dir = self.__inspect(out_dir, "--out-dir")
        self.out_dir.mkdir(exist_ok=True)
        self.metadata = {"queries": self.__read_yaml(metadata)}
        self.spacy = spacy.load(model)
        # TODO: la eliminación o sobreescritura de archivos previos debería ser
        # opcional
        for file in self.out_dir.glob("*.*"):
            file.unlink()
        for xml in self.in_dir.rglob("*.xml"):
            self.__verticalize(xml)
        self.__write_meta(metadata)

    def __inspect(self, folder: Path, par: str) -> Path:
        """Inspecciona la validas de la ruta a un directorio
        TODO: debería estar en commons

        :param folder: Ruta al directorio
        :type folder: Path
        :param par: Nombre del parámetro
        :type par: str
        :return: Ruta relativa al directorio actual de trabajo
        :rtype: Path
        """
        if not folder.stem:
            self.log.puts(f"[{par}] Parámetro necesario", level="error")
        if not folder.exists():
            self.log.puts(f"[{folder}] Inexistente", level="error")
        if not folder.is_dir():
            self.log.puts(f"[{folder}] No es un directorio", level="error")
        return Path(folder).resolve()

    def __read_file(self, path: Path) -> str:
        """Intenta parsear un archivo
        TODO: debería estar en commons y fusionado con __inspect

        :param path: Ruta al archivo
        :type path: Path
        """
        if not path.exists():
            self.log.puts(f"[{path}] Inexistente", level="error")
        if not path.is_file():
            self.log.puts(f"[{path}] No es un archivo", level="error")
        return path.read_text()

    def __read_yaml(self, path: Path) -> dict:
        """Intenta parsear un archivo YAML
        TODO: debería estar en commons

        :param path: Ruta al archivo
        :type path: Path
        """
        raw = self.__read_file(path)
        if raw:
            return yaml.safe_load(raw)
        else:
            return {}

    def __verticalize(self, path: Path) -> None:
        """Convierte, verifica y escribe archivo XML a VRT

        :param path: Ruta al archivo XML
        :type path: Path
        """
        self.log.puts(f"[{path}] Verticalizando")
        try:
            soup = BeautifulSoup(path.read_text(), features="xml")
            content = self.__select(soup)
            tokenized = self.__tokenize(content, [])
            vertical = self.__dump(tokenized)
            ET.fromstring(vertical)
            file = self.out_dir / f"{path.stem}.vrt"
            self.log.puts(f"[{file}] Guardando")
            file.write_text(vertical)
            if bool(self.metadata["queries"]):
                self.__add_meta(soup)
        except ET.ParseError as error:
            self.log.puts(f"[{path}] Estructura inválida: {error}", level="warn")
            self.__dbg(vertical, error.position[0])
            self.log.puts(f"[{path}] No se verticalizó", level="warn")
        except Exception as error:
            self.log.puts(f"[{path}] Error: {error}", level="warn")
            self.log.puts(f"[{path}] No se verticalizó", level="warn")

    def __find_meta(self, soup: Tag, name: str, args: dict) -> str:
        """Busca metadato del archivo

        :param soup: Archivo como árbol sintáctico
        :type soup: Tag
        :param name: Nombre del metadato
        :type name: str
        :param args: Argumentos para BeautifulSoup
        :type args: dict
        :return: Valor del metadato
        :rtype: str
        """
        default = "NA"
        tag = args["tag"]
        if soup:
            if "attr" in args:
                res = soup.find(tag, attrs={args["attr"]: True})
                meta = res[args["attr"]] if res else default
            else:
                res = soup.find(tag)
                meta = res.string if res else default
        else:
            msg = f"[{name}] Etiqueta no encontrada para metadato;"
            self.log.puts(
                f"{msg} consulta realizada con: {args}",
                level="warn",
            )
            meta = default
        return meta.strip()

    def __add_meta(self, soup: Tag) -> None:
        """Añade metadato del archivo

        :param soup: Archivo como árbol sintáctico
        :type soup: Tag
        """
        row = []
        head = list(self.metadata["queries"].keys())
        head = list(map(lambda x: f"Field{head.index(x)}\t{x}", head))
        self.metadata.setdefault("list", [])
        self.metadata.setdefault("head", head)
        for name, args in self.metadata["queries"].items():
            if "root" in args:
                meta = self.__find_meta(soup.find(args["root"]), name, args)
            else:
                meta = self.__find_meta(soup, name, args)
            row.append(meta.strip())
        self.metadata["list"].append(row)

    def __write_meta(self, metadata: Path) -> None:
        """Escribe el archivo con los metadatos. Escribe los metadatos en un
        archivo TXT en la misma ubicación al YAML

        :param metadata: Ruta a los metadatos YAML
        :type metadata: Path
        """
        if bool(self.metadata["queries"]):
            del self.metadata["queries"]
            for key, val in self.metadata.items():
                ext = "tsv" if key == "list" else "txt"
                out_meta = metadata.parent / f"{metadata.stem}_{key}.{ext}"
                out_meta = out_meta.resolve()
                self.log.puts(f"[{out_meta}] Guardando")
                if ext == "tsv":
                    with open(out_meta, "w") as file:
                        tsv = csv.writer(file, delimiter="\t")
                        for row in val:
                            tsv.writerow(row)
                else:
                    out_meta.write_text("\n".join(val))

    def __select(
        self,
        soup,
        content_tag: str = "cuerpo",
        id_pos: tuple = ("archivo_palabra", "apal_id"),
    ):
        text_id = soup.find(id_pos[0]).attrs[id_pos[1]]
        text = soup.find(content_tag)
        text.name = "text"
        text["id"] = text_id
        return text.wrap(soup.new_tag("root"))

    def __tokenize(
        self,
        soup: Tag,
        events: list = [],
        level: int = 0,
        withcontent: tuple = ["p", "nrp"],
        ischild: bool = False,
        addend: bool = False,
    ) -> Tag:
        cols = "form lemma tag morph".strip().split()
        self.metadata.setdefault("p-attributes", cols[1:])
        for tag in soup.children:
            if isinstance(tag, NavigableString):
                string = str(tag.string).strip()
                if ischild or tag.name in withcontent:
                    tokens = self.spacy(string)
                    for i, token in enumerate(tokens):
                        form = token.text
                        lemma = token.lemma_.lower()
                        tag = token.tag_.lower()
                        morph = str(token.morph) if token.morph else "?"
                        cols = "\t".join([form, lemma, tag, morph])
                        if token.is_sent_start:
                            events.append(Event(level, "INITAG", "s"))
                        events.append(Event(level, "TOKEN", cols))
                        if token.is_sent_end:
                            events.append(Event(level, "ENDTAG", "s"))
                else:
                    if string:
                        events.append(Event(level, "STRING", string))
            else:
                data = [level + 1, withcontent, ischild, addend]
                if tag.name in withcontent:
                    data[2] = True
                events.append(Event(level, "INITAG", tag.name, tag.attrs))
                events = self.__tokenize(tag, events, *data)
                events.append(Event(level, "ENDTAG", tag.name))
        return events

    def __dbg(self, vertical: str, inline: int) -> None:
        lines = vertical.split("\n")
        ini = max(0, inline - 10)
        end = min(len(lines), inline + 10)
        for i, line in enumerate(lines):
            index = i + 1
            if index >= ini and index <= end:
                num = "[" + str(index).zfill(len(str(end))) + "]"
                num += "->" if index == inline else "  "
                self.log.puts(f"{num}{line}")

    def __dump(self, tokens: list[Event]) -> str:
        """Vierte eventos como cadena verticalizada

        :param tokens: Lista de eventos tokenizada
        :type tokens: list[Event]
        :return: Cadena verticalizada
        :rtype: str
        """
        values = []
        for i, event in enumerate(tokens):
            match event.type:
                case "INITAG":
                    attrs = ""
                    for key, val in event.attrs.items():
                        attrs += f' {key}="{val}"'
                    values.append(f"<{event.value}{attrs}>")
                case "TOKEN" | "STRING":
                    values.append(bleach.clean(event.value))
                case "ENDTAG":
                    values.append(f"</{event.value}>")
        return "\n".join(values)


if __name__ == "__main__":
    out_root = Path(__file__).parent / ".." / ".." / "apal-vrt"
    Verticalize(
        in_dir=Path(__file__).parent / ".." / ".." / "apal-xml" / "xml",
        out_dir=out_root / "vrt",
        metadata=out_root / "metadata.yaml",
    )
