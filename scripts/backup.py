#!/usr/bin/env python
# Respaldo de XML de Archivo de la Palabra
# (c) 2023 perro tuerto <hi@perrotuerto.blog>
# Financiado por la Academia Mexicana de la Lengua <https://academia.org.mx>
# Este código es software libre con licencia GPLv3
# python scripts/backup.py

import shutil
from common import Log
from gworkspace import G
from pathlib import Path


class Backup:
    """Realiza respaldos de archivos XML almacenados en GDrive. Para evitar
    sobreescritura, el respaldo previo es eliminado

    :param gid: Id del directorio de GDrive
    :type gid: str
    :param cred_dir: Ruta al directorio con las credenciales de GDrive
    :type cred_dir: Path
    :param bak_dir: Ruta al directorio para el respaldo
    :type bak_dir: Path
    :param quiet: Indica si imprime el registro, False por defecto
    :type quiet: bool, opcional
    :param write: Indica si guarda el registro en un archivo, False por defecto
    :type write: bool, opcional
    """

    def __init__(
        self,
        gid: str,
        cred_dir: Path,
        bak_dir: Path,
        quiet: bool = False,
        write: bool = False,
    ) -> None:
        self.log = Log(quiet=quiet, write=write)
        self.bak_dir = Path(bak_dir)
        if gid:
            self.g = G(gid=gid, cred_dir=cred_dir)
        else:
            self.log.puts("[--gid] Parámetro necesario", level="error")
        self.__clean()
        self.__save(gid, self.bak_dir)
        self.log.puts(f"[{self.bak_dir}] Respaldo terminado")
        self.log.puts(f"[{self.bak_dir}] Archivos respaldados: {self.g.total}")

    def __clean(self) -> None:
        """Limpia directorio para el respaldo. Elimina los archivos para evitar
        sobreescritura
        """
        if not self.bak_dir.exists():
            self.bak_dir.mkdir()
        for child in self.bak_dir.glob("*"):
            if child.name not in ["tests", ".git", ".gitignore"]:
                try:
                    child.unlink()
                except Exception:
                    shutil.rmtree(child)

    def __save(self, gid: str, root: Path) -> None:
        """Itera los directorios de GDrive para llevar a cabo el guardado

        :param gid: Identificador del directorio de GDrive
        :type gid: str
        :param root: Ruta para guardar los archivos
        :type root: Path
        """
        items = self.g.list(gid)
        if items:
            for item in items:
                if item["type"] == "folder":
                    self.__save(item["id"], root / item["name"])
                else:
                    self.__save_file(item, root)

    def __save_file(self, item: dict, root: Path) -> None:
        """Guarda el archivo. El ítem de GDrive es un diccionario que contiene
        información del archivo, este se utiliza para su guardado local

        :param item: Ítem de GDrive
        :type item: dict
        :param root: Ruta para guardar el archivo
        :type root: Path
        """
        path = (root / item["name"]).resolve()
        try:
            path = path.relative_to(Path.cwd())
        except ValueError:
            pass
        root.mkdir(exist_ok=True)
        self.g.download(path, item)


if __name__ == "__main__":
    Backup(
        gid="1mKobFGdoedQ92oaPsgg4Ul7s0aHosGhB",
        cred_dir=Path(__file__).parent / ".." / "assets",
        bak_dir=Path(__file__).parent / ".." / ".." / "apal-docs",
    )
