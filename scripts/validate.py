#!/usr/bin/env python
# Validación de XML de Archivo de la Palabra
# (c) 2023 perro tuerto <hi@perrotuerto.blog>
# Financiado por la Academia Mexicana de la Lengua <https://academia.org.mx>
# Este código es software libre con licencia GPLv3
# python scripts/validate.py

import csv
import xml.parsers.expat as expat
from common import Log
from pathlib import Path
from typing import NoReturn
from bs4 import BeautifulSoup
from xml.etree.ElementTree import ParseError
from xmlschema import XMLSchema10, XMLSchemaValidationError


class Validate:
    """Realiza validaciones a los archivos XML. Los archivos pueden tener
    extensión '.xml' o '.txt', o carecer (arroja advertencia)

    :param xsd: Ruta al esquema XSD, str por defecto
    :type xsd: Path
    :param in_dir: Directorio con los XML para validar, permite subdirectorios,
        str por defecto
    :type in_dir: Path
    :param out_dir: Directorio para los XML validados, str por defecto
    :type out_dir: Path
    :param quiet: Indica si imprime el registro, False por defecto
    :type quiet: bool, opcional
    :param write: Indica si guarda el registro en un archivo, False por defecto
    :type write: bool, opcional
    """

    def __init__(
        self,
        xsd: Path | str = "",
        in_dir: Path | str = "",
        out_dir: Path | str = "",
        quiet: bool = False,
        write: bool = False,
    ) -> None:
        self.files: dict[str, Path] = {}
        self.keep: dict[str, Path] = {}
        self.res: list = []
        self.xsd: XMLSchema10
        self.log = Log(quiet=quiet, write=write)
        self.cwd, self.in_dir, self.out_dir = Path.cwd(), Path(), Path()
        self.__inspect(Path(xsd), Path(in_dir), Path(out_dir))
        self.files = self.__get_files()
        self.__validate()

    def __inspect(self, xsd: Path, in_dir: Path, out_dir: Path) -> None:
        """Inspecciona que las rutas al esquema XSD y a los directorios de
        trabajo sean rutas válidas

        :param in_dir: Directorio con los XML para validar, permite
            subdirectorios, str por defecto
        :type in_dir: Path
        :param out_dir: Directorio para los XML validados, str por defecto
        :type out_dir: Path
        :param xsd: Ruta al esquema XSD
        :type xsd: Path
        """
        for i, rawpath in enumerate([xsd, in_dir, out_dir]):
            name = ["--xsd", "--in-dir", "--out-dir"][i]
            try:
                path = rawpath.resolve().relative_to(self.cwd)
            except ValueError:
                path = rawpath.resolve()
                pass
            if not path.stem:
                self.log.puts(f"[{name}] Parámetro necesario", level="error")
            elif not path.exists():
                self.log.puts(f"[{path}] Inexistente", level="error")
            elif path.suffix != ".xsd" and not path.is_dir():
                self.log.puts(f"[{path}] No es un directorio", level="error")
            elif path.suffix == ".xsd" and not path.is_file():
                self.log.puts(f"[{path}] No es un archivo", level="error")
            else:
                attr = name[2:].replace("-", "_")
                value = i == 0 and self.__get_xsd(path) or path
                setattr(self, attr, value)

    def __validate(self) -> None:
        """Realiza las validaciones correspondientes"""
        self.__validate_quantity()
        self.__validate_common("duplicated")
        self.__validate_common("structure")
        self.__write_files()

    def __validate_quantity(self) -> None:
        """Valida cantidad de XML. Si los directorios están nombradas con un
        intervalo (p. ej. `1-100`), obtiene la cantidad de archivos que según
        debe tener el directorio. De lo contrario ignora el directorio
        """
        self.res, dirs = [], set(map(lambda p: p.parent, self.files.values()))
        for path in dirs:
            interval = self.__get_quantity_interval(path)
            if interval[0] != 0 and interval[1] != 0:
                self.res.append(self.__validate_quantity_files(path, interval))
        self.__write_csv("cantidad")

    def __validate_common(self, kind: str) -> None:
        """Función común para las validaciones de duplicados y de estructura

        :param kind: Tipo de validacion entre "duplicated" o "structure"
        :type kind: str
        """
        self.res, self.keep, level, head = [], {}, "info", self.in_dir
        msg = "duplicados" if kind == "duplicated" else "inválidos"
        csv = "duplicados" if kind == "duplicated" else "estructura"
        for k, v in self.files.items():
            if kind == "duplicated":
                self.__validate_duplicated_file(k, v)
            else:
                self.__validate_structure_file(k, v)
        if self.files != self.keep:
            remove = set(self.files) - set(self.keep)
            for file in remove:
                del self.files[file]
            msg, level = f"{msg}: {len(remove)}", "warn"
            self.log.puts(f"[{head}] XML", msg, level=level)
        self.log.puts(f"[{head}] XML restantes:", len(self.files), level=level)
        self.__write_csv(csv)

    def __validate_quantity_files(self, path: Path, interval: tuple) -> list:
        """Valida cantidad de XML en cada directorio

        :param path: Ruta al directorio
        :type path: Path
        :param interval: Intervalo de archivos en el directorio
        :type interval: tuple
        :return: Resultados de la validación
        :type: list
        """
        name = str(path)
        expected = (interval[1] - interval[0]) + 1
        files = list(filter(lambda x: x.parent == path, self.files.values()))
        diff = -(expected - len(files))
        if diff != 0:
            self.log.puts(f"[{name}] Cantidad inválida: {diff}", level="warn")
        return [name, expected, len(files), diff]

    def __validate_duplicated_file(self, key: str, filepath: Path) -> None:
        """Valida archivo duplicado

        :return: Archivos a mantener
        :rtype: dict[str, Path]
        """
        stem = str(filepath.stem)
        stems = dict(map(lambda e: (str(e[1].stem), e[1]), self.keep.items()))
        if stem in stems:
            file1 = filepath
            file2 = stems[stem]
            msg = f"[{file1}] duplicado de: {file2}"
            self.res.append([file1, file2])
            self.log.puts(msg, level="warn", clean=True)
        else:
            self.keep[key] = filepath

    def __validate_structure_file(self, key: str, path: Path) -> None:
        """Valida estructura de archivo

        :param key: Nombre de la llave del archivo
        :type ket: str
        :param path: Ruta al archivo
        :type path: Path
        :return: Archivos a mantener
        :rtype: dict[str, Path]
        """
        try:
            self.xsd.validate(path)
            content = BeautifulSoup(path.read_text(), features="xml")
            apal_id = content.find(apal_id=True)["apal_id"]
            if apal_id != path.stem:
                raise ValueError("doc ID does not match doc file name")
            self.keep[key] = path
            self.log.puts(f"[{path}] válido", clean=True)
        except ParseError as error:
            msg = list(expat.errors.codes.keys())[error.code]
            pos = error.position
            msg = f"[{path}] inválido {str(pos)}: {msg}"
            self.log.puts(msg, level="warn", clean=True)
            self.res.append([path, "Sintáctico", pos[0], pos[1], msg])
        except XMLSchemaValidationError as error:
            msg = str(error.reason)
            pos = (-1, -1)
            msg = f"[{path}] inválido {str(pos)}: {msg}"
            self.log.puts(msg, level="warn", clean=True)
            self.res.append([path, "Semántico", pos[0], pos[1], msg])
        except ValueError as error:
            msg = str(error)
            pos = (-1, -1)
            msg = f"[{path}] inválido {str(pos)}: {msg}"
            self.log.puts(msg, level="warn", clean=True)
            self.res.append([path, "Semántico", pos[0], pos[1], msg])

    def __get_xsd(self, path: Path) -> XMLSchema10 | NoReturn:
        """Obtiene el XSD a partir de su ruta. Si falla, arroja error y aborta

        :param path: Ruta al directorio
        :type path: Path
        :return: XSD
        :rtype: XMLSchema10
        """
        try:
            xsd = XMLSchema10(path)
            self.log.puts(f"[{path}] XSD válido")
            return xsd
        except Exception:
            raise Exception(
                self.log.puts(f"[{path}] XSD inválido", level="error")
            )

    def __get_files(self) -> dict[str, Path]:
        """Obtiene archivos XML a partir de la ruta a su directorio

        :return: Rutas a los archivos
        :rtype: dict[str, Path]
        """
        files = {}
        valid_suffixes = ["", ".XML", ".xml", ".txt"]
        for child in self.in_dir.rglob("*"):
            string = str(child)
            if string == string.replace(".git", "") and child.name[0] != ".":
                if child.is_file() and child.suffix in valid_suffixes:
                    files[str(child)] = child
                if child.is_file() and not child.suffix:
                    short = child.name
                    self.log.puts(f"[{short}] XML sin extensión", level="warn")
        self.log.puts(f"[{self.in_dir}] XML encontrados: {len(files)}")
        return files

    def __get_template(self, name: str) -> dict:
        """Obtiene plantilla CSV. La plantilla asigna el directorio 'informes'
        en el directorio destino

        :param name: Nombre de la plantilla
        :type name: str
        :return: Plantilla CSV
        :rtype: dict
        """
        root = self.out_dir / "informes"
        if not root.exists():
            root.mkdir()
            self.log.puts(f"[{root}] Creado para guardar las validaciones")
        if root.is_file():
            self.log.puts(f"[{root}] No es un directorio", level="error")
        return {
            "filename": root / f"{name}.csv",
            "head": self.__get_template_head(name),
        }

    def __get_template_head(self, name: str) -> list:
        """Obtiene encabezados de la plantilla CSV

        :param name: Nombre de la plantilla
        :type name: str
        :return: Encabezados CSV
        :rtype: list
        """
        return {
            "cantidad": ["Dir", "XML esperados", "XML encontrados", "Diff"],
            "estructura": ["XML", "Error", "Línea", "Columna", "Mensaje"],
            "duplicados": ["XML en ubicación 1", "XML en ubicación 2"],
        }[name]

    def __get_quantity_interval(self, path: Path) -> tuple[int, int]:
        """Obtiene intervalo de archivos en un directorio. Si falla, arroja
        advertencia

        :param path: Ruta al directorio
        :type path: Path
        :return: Intervalo de archivos
        :rtype: tuple[int, int]
        """
        splitted = path.stem.split("-")
        mapped = map(lambda x: int(x) if x.isnumeric() else None, splitted)
        tupled = tuple(filter(None, list(mapped)))
        if len(tupled) == 2 and tupled[0] < tupled[1]:
            parts = (tupled[0], tupled[1])
        else:
            self.log.puts(f"[{path}] Carece de intervalo", level="warn")
            parts = (0, 0)
        return parts

    def __write_csv(self, name: str) -> None:
        """Guarda validación en un CSV

        :param name: Nombre de la plantilla
        :type name: str
        """
        template = self.__get_template(name)
        with open(template["filename"], "w") as filename:
            file = csv.writer(filename, dialect="unix")
            file.writerow(template["head"])
            file.writerows(self.res)

    def __write_files(self) -> None:
        """Guarda archivos válidos en XML embellecido"""
        for key in self.files:
            inpath = self.files[key]
            outdir = self.out_dir / "xml"
            outpath = outdir / inpath.name
            content = BeautifulSoup(inpath.read_text(), features="xml")
            outdir.mkdir(exist_ok=True)
            outpath.write_text(content.prettify())


if __name__ == "__main__":
    Validate(
        xsd=Path(__file__).parent / ".." / ".." / "apal-xsd" / "apal.xsd",
        in_dir=Path(__file__).parent / ".." / ".." / "apal-docs",
        out_dir=Path(__file__).parent / ".." / ".." / "apal-xml",
    )
