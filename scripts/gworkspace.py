#!/usr/bin/env python
# Servicios de Google para Archivo de la Palabra
# Basado en el tutorial básico para usar Google Workspace API:
#   https://developers.google.com/drive/api/quickstart/python
# (c) 2023 perro tuerto <hi@perrotuerto.blog>
# Financiado por la Academia Mexicana de la Lengua <https://academia.org.mx>
# Este código es software libre con licencia GPLv3

import io
import shutil
from common import Log
from pathlib import Path
from typing import NoReturn
from google.auth.transport.requests import Request  # type: ignore
from google.oauth2.credentials import Credentials  # type: ignore
from google_auth_oauthlib.flow import InstalledAppFlow  # type: ignore
from googleapiclient.http import MediaIoBaseDownload  # type: ignore
from googleapiclient.errors import HttpError  # type: ignore
from googleapiclient.discovery import build  # type: ignore


class G:
    """Establece conexión con GDrive

    :param gid: Id del directorio de GDrive
    :type gid: str
    :param cred_dir: Ruta al directorio con las credenciales
    :type cred_dir: Path
    :param quiet: Indica si imprime el registro, False por defecto
    :type quiet: bool, opcional
    :param write: Indica si guarda el registro en un archivo, True por defecto
    :type write: bool, opcional
    """

    def __init__(
        self,
        gid: str,
        cred_dir: Path,
        quiet: bool = False,
        write: bool = True,
    ) -> None:
        self.log = Log(quiet=quiet, write=write)
        self.scopes = ["https://www.googleapis.com/auth/drive"]
        self.cred_dir = Path(cred_dir)
        self.credspath = self.cred_dir / "credentials.json"
        self.token = self.cred_dir / "token.json"
        self.total = 0
        self.__inspect()
        self.log.puts("[gdrive] Iniciando conexión a GDrive")
        self.service = build("drive", "v3", credentials=self.creds)

    def list(self, gid: str) -> dict | NoReturn:
        """Obtiene lista de archivos dentro de directorio de GDrive

        :param gid: Id del directorio de GDrive
        :type gid: str
        """
        try:
            results = (
                self.service.files()
                .list(
                    q=f"'{gid}' in parents",
                    supportsAllDrives=True,
                    includeItemsFromAllDrives=True,
                    pageSize=1000,
                    fields="nextPageToken, files(id, name, mimeType)",
                )
                .execute()
            )
            return self.__clean_type(results.get("files", []))
        except HttpError as error:
            raise HttpError(self.log.puts(f"[gdrive] {error}", level="error"))

    def download(self, path: Path, item: dict) -> None:
        """Descarga archivo en ruta destino

        :param path: Ruta destino
        :type path: Path
        :param item: Ítem de GDrive
        :type item: dict
        """
        self.total += 1
        if item["type"] == "gfile":
            self.__download_gfile(path, item)
        else:
            self.__download_binary(path, item)

    def __inspect(self) -> None:
        """Inspecciona que las credenciales sean válidas

        TODO: Probablemente necesita hacerse debug
        """
        if not self.credspath.exists():
            self.cred_dir.mkdir(exist_ok=True)
            self.log.puts(
                f"[gdrive] Se requiere el archivo {self.token}, consulta:",
                "https://developers.google.com/drive/api/quickstart/python",
                level="error",
            )
        if self.token.exists():
            self.creds = Credentials.from_authorized_user_file(
                self.token, self.scopes
            )
        if not self.creds.valid:
            self.__gen_creds()

    def __gen_creds(self) -> None:
        """Genera credenciales

        TODO: Probablemente necesita hacerse debug
        """
        self.log.puts("[gdrive] Solicitando nuevas credencias a Google")
        if self.creds and self.creds.expired and self.creds.refresh_token:
            self.creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                self.creds, self.scopes
            )
            self.creds = flow.run_local_server(port=0)
        with open(self.token, "w") as token:
            token.write(self.creds.to_json())

    def __clean_type(self, items: dict) -> dict:
        """Limpia el tipo MIME al ítem de GDrive. Los ítems tienen los tipo
        MIME de los formatos de Google, esto limpia el MIME para su posterior
        evaluación

        :param item: Ítem de GDrive
        :type item: dict
        :return: Ítem de GDrive limpiado
        :rtype: dict
        """
        for item in items:
            mime = item["mimeType"]
            ftype = mime.replace("application/vnd.google-apps.", "")
            if ftype == ftype.replace("/", "") and ftype != "folder":
                item["mimeType"] = ftype
                ftype = "gfile"
            item["type"] = ftype
        return items

    def __get_type(self, gtype: str) -> dict:
        """Obtiene la extensión de archivo y el tipo MIME. Los ítems de GDrive
        tienen MIME de los formatos de Google, además de carecer de extensión

        :param gtype: Tipo de archivo de GDrive
        :type gtype: str
        :return: Extensión de archivo y tipo de MIME
        :rtype: dict
        """
        return {
            "spreadsheet": {"ext": ".csv", "mime": "text/csv"},
            "document": {
                "ext": ".odt",
                "mime": "application/vnd.oasis.opendocument.text",
            },
            "presentation": {
                "ext": ".odp",
                "mime": "application/vnd.oasis.opendocument.presentation",
            },
            "drawing": {"ext": ".svg", "mime": "image/svg+xml"},
            "script": {
                "ext": ".json",
                "mime": "application/vnd.google-apps.script+json",
            },
        }[gtype]

    def __start_download(self, path: Path, request: Request) -> None:
        """Comienza la descarga del archivo

        :param path: Ruta para la descarga
        :type path: Path
        :param request: Solicitud de descarga
        :type request: Request
        """
        fh = io.BytesIO()
        downloader = MediaIoBaseDownload(fh, request, chunksize=204800)
        done = False
        try:
            while not done:
                status, done = downloader.next_chunk()
                curr = int(status.resumable_progress * 100 / status.total_size)
                msg = f"[{path}] Descargando archivo {self.total}: {curr}%"
                self.log.puts(msg, clean=True)
            fh.seek(0)
            with open(path, "wb") as f:
                shutil.copyfileobj(fh, f)
        except Exception as error:
            raise Exception(self.log.puts(f"[gdrive] {error}", level="error"))

    def __download_binary(self, path: Path, item: dict) -> None:
        """Descarga archivo binario. Los archivos binarios son los que no son
        formatos nativos de Google

        :param path: Ruta para la descarga
        :type path: Path
        :return: Ítem de GDrive
        :rtype: dict
        """
        request = self.service.files().get_media(fileId=item["id"])
        self.__start_download(path, request)

    def __download_gfile(self, path: Path, item: dict) -> None:
        """Descarga archivo de GDrive. Los archivos nativos de Google requieren
        un trato diferente para su descarga

        :param path: Ruta para la descarga
        :type path: Path
        :return: Ítem de GDrive
        :rtype: dict
        """
        types = self.__get_type(item["mimeType"])
        request = self.service.files().export(
            fileId=item["id"],
            mimeType=types["mime"],
        )
        self.__start_download(path.with_suffix(types["ext"]), request)
