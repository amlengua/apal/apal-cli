# Apal CLI

Herramienta [CLI] para la gestión del corpus del Archivo de la Palabra, un
proyecto de la [Academia Mexicana de la Lengua].

\[\[*TOC*\]\]

## Configuración

Para que funcionen los *scripts* es necesario lo siguiente.

Genera un ambiente virtual:

    virtual .venv

Activa el ambiente virtual:

    source .venv/bin/activate

Instala requisitos:

    pip install -r requirements.txt

## Uso de scripts

### Respaldo

Ejecuta:

    python ./scripts/backup.py

### Validación

Ejecuta:

    python ./scripts/validate.py

### Verticalización

Ejecuta:

    python ./scripts/verticalize.py

## Uso de CQPweb

![Ejemplo de consulta.]

Para poder realizar consultas en el corpus es necesario:

1.  Habilitar escritura de archivos en la base de datos.
2.  Subir los archivos VRT.
3.  Instalar un nuevo corpus.
4.  Establecer accesos al corpus.
5.  Generar los metadatos del corpus.

### Habilitación de escritura en la base de datos

Este proceso permite cargar datos locales a la base de datos y **solo tiene que
hacerse una vez** cuando recién se ha instalado el corpus.

1.  Ingresa al servidor a través de SSH (requieres acceso).
2.  Ingresa al contenedor del corpus con
    `sh ~/repositorios/respaldo-cqpweb/scripts/enter.sh`.
3.  Ingresa a la base de datos del corpus con `mysql`.
4.  Ejecuta esta consulta `SET GLOBAL local_infile = 'ON';`.
5.  Salte de la base de datos con `\q`.
6.  Salte del contenedor con `exit`.
7.  Salte del servidor con `exit`.
8.  ¡Listo!

### Subida de archivos

1.  En el menú izquierdo del panel de administracción vea a
    `Corpora > View upload area`.
2.  Escoge cada archivo con el botón de `Examinar…`.
3.  Haz clic en `Upload file`.
4.  ¡Listo!

### Instalación de nuevo corpus

1.  En el menú izquierdo del panel de administracción ve a
    `Corpora > Install new corpus`.
2.  Escoge un nombre en minúsculas, sin tildes ni espacios en `name`.
3.  Incluye una descripción en `full descriptive name`.
4.  Escoge los archivos para el corpus.
5.  Añade los atributos S, donde `Element tag` es cada una de las etiquetas que
    se usan y `Description` es la descripción de la etiqueta; opcionalmente
    añade atributos en `Attr tag`, su descripción en `Description` y
    `Free text` como `Datatype`.
6.  Añade los atributos P, donde `Handle` es cada nombre de la columna de cada
    palabra, ignorando la primera columa porque por defecto es la forma
    ortográfica con `word` como `Handle`, y `Description` es la descripción de
    la columna.
7.  Haz clic en `Install corpus with settings above`.
8.  ¡Listo!

Lo más confuso de este paso es la adición de los atributos, así que aquí está
una imagen ilustrativa:

![Configuración de atributos S basados en el esquema XML.]

### Establecimiento de accesos al corpus

1.  En el menú izquierdo del panel de administracción ve a
    `Users and privileges > Manage privileges`.
2.  Ve a `Generate default corpus privileges` que está hasta el fondo.
3.  Selecciona el corpus.
4.  Haz clic en `Generate default privileges for this corpus`.

Esto ahora permite otorgar permiso por usuario o por grupos.

Para otorgar permisos por usuario, haz:

1.  En el menú izquierdo del panel de administracción ve a
    `Users and privileges > Manage user grants`.
2.  Selecciona usuario y tipo de privilegio.
3.  Haz clic en `Grant privilege to user!`.
4.  ¡Listo!

Para otorgar permisos por grupo, haz:

1.  En el menú izquierdo del panel de administracción ve a
    `Users and privileges > Manage group grants`.
2.  Selecciona grupo y tipo de privilegio.
3.  Haz clic en `Grant privilege to group!`.
4.  ¡Listo!

### Generación de metadatos del corpus

1.  En el menú izquierdo del panel de administracción ve a
    `Corpora > Show corpora`.
2.  Haz clic en el corpus al que se desea generar los metadatos.

Esto llevará a un nuevo panel del corpus, en donde ahora:

1.  En el menú izquierdo del panel del corpus ve a `Manage text metadata`.
2.  Ve a `Click here to install metadata from within-corpus XML annotation` que
    está hasta el fondo.
3.  Selecciona los atributos de la etiqueta `text` de cada VRT si los tiene;
    **nunca** selecciones atributos que no comiencen con `text_`.
4.  Selecciona
    `Yes please, run this automatically (ideal for relatively small corpora)`.
5.  Haz clic en `Create metadata table from XML using the settings above`.
6.  ¡Listo!

### Consultas al corpus

1.  En el menú izquierdo del panel del corpus ve a `Standard query`.
2.  Escribe tu consulta; no olvides consultar la [sintaxis básica para las
    consultas].
3.  Haz clic en `Start Query`.
4.  ¡Listo!

## Referencias de utilidad

-   [Sitio con la documentación de CWB y CQPweb]
-   [Manual de administración de CQPweb]
-   [Manual de codificación de corpus y gestión de CWB]
-   [Sintaxis básica para consultas en
    CQPweb][sintaxis básica para las consultas]
-   [Documentación de INCEpTION]

## Tareas por hacer

-   [x] Elaborar esquema XML (XSD).
-   [x] Elaborar validaciones semánticas.
-   [ ] Revisar cómo hacer la lista de frecuencias para corpus grandes.
-   [ ] Desarrollar CLI para hacer todos los procesos.

  [CLI]: https://es.wikipedia.org/wiki/Interfaz_de_l%C3%ADnea_de_comandos
  [Academia Mexicana de la Lengua]: https://academia.org.mx/
  [Ejemplo de consulta.]: assets/corpus_query.png
  [Configuración de atributos S basados en el esquema XML.]: assets/corpus_conf.png
  [sintaxis básica para las consultas]: http://89.58.54.98:8282/doc/cqpweb-simple-syntax-help.pdf
  [Sitio con la documentación de CWB y CQPweb]: https://cwb.sourceforge.io/documentation.php
  [Manual de administración de CQPweb]: https://cwb.sourceforge.io/files/CQPwebAdminManual.pdf
  [Manual de codificación de corpus y gestión de CWB]: https://cwb.sourceforge.io/files/CWB_Encoding_Tutorial.pdf
  [Documentación de INCEpTION]: https://inception-project.github.io/releases/25.5/docs/user-guide.html#_workflow
